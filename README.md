# Generic Spring Boot Projects

This repository provides an overview of two (currently) projects that play a crucial role in simplifying and accelerating the development of Spring Boot servers.

These highly flexible and configurable projects can serve as the foundational components for much more complex systems.

The main components of the Generic Spring Boot Project include:

- [Igor Kostin / Generic Spring Boot - Generics · GitLab](https://gitlab.com/Igor.Kostin/generics) - A versatile and flexible Spring Boot project with generic classes and utilities. 

- [Generic Spring Boot / Processor · GitLab](https://gitlab.com/generic-spring-boot/processor) - A code generation module for rapid Spring Boot server development.

### The Story

The Generic Spring Boot project was conceived as a multi-layered solution. In its initial layer, known as [Generics](https://gitlab.com/Igor.Kostin/generics), the goal was to create a straightforward project containing essential utilities and generic classes for establishing a fully generic Spring server. However, a slight complication persisted: developers still had to manually declare five classes for each entity (DTO, Repository, Service, Controller, and Entity). Even though these five classes were mostly extensions of generic templates, they required explicit declaration.

Inspired by projects like [Project Lombok](https://projectlombok.org/) (which allows for compiling and working with code that doesn't exist in source form), a new idea emerged. This led to the addition of another layer within the Generic Spring Boot project, known as the [Processor](https://gitlab.com/generic-spring-boot/processor). This relatively simple project harnesses the power of Java annotations and annotation processing to automatically generate missing DTOs, Repositories, Services, and Controllers— all of which are inherently generic. 

### The General Idea

The overarching goal was to create a solution for rapidly developing fully functional CRUD Spring Boot applications. This approach aimed to make application development as data-driven as possible, where developers could simply declare their data models,  entities and their relationships.

By leveraging the processor and its capabilities, the remaining missing classes (DTO, Repository, Service, Controller) will be automatically generated, compiled, and ready for use.

However, it's important to note that each of these classes could still be implemented in a manner compatible with generics. This flexibility allowed developers to re-implement any specific methods they required, whether it was in the Repository, Service, Controller, or even DTO classes, if there were unique needs to address.

The result was a fully functional CRUD Spring Boot server that could be tailored to individual needs in the shortest amount of time possible.  

### The Purpose

Originally conceived as a means to pass an exam in the shortest time possible, this project has evolved to become a powerful tool for rapid application development. Potential use cases include:

- **Streamlined Development:** Simplify and accelerate the creation of Spring Boot applications, especially those involving CRUD operations on data entities.

- **Data-Driven Development:** Promote a data-driven approach to application development, enabling developers to focus on defining data models without being weighed down by boilerplate code.

- **Generics and Automation:** Harness the power of generics and code generation to automate the creation of common components like DTOs, Repositories, Services, and Controllers.

- **Customizability:** Offer developers the flexibility to customize and extend generated code while benefiting from automation.

- **Rapid Prototyping:** Facilitate rapid prototyping and the development of fully functional Spring Boot servers, reducing development time and effort. This allows other teams to commence their development work while servers are still in their early stages.

### The Use

Check out the [Generic Spring Boot / Demo-PersonRentingBook](https://gitlab.com/generic-spring-boot/demo-personrentingbook) demo project I have created to see the solution in a real-world example.

In short, it's a fully functional CRUD Spring Boot application created from just three entities, a few dependencies, and a bunch of background processing. 😁
